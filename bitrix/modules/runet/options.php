<?php

	use Bitrix\Main\Config\Option;

	$module_id = 'runet';
	$action_url = $APPLICATION->GetCurPage().'?mid='.htmlspecialchars($mid).'&amp;lang='.LANG;

	CModule::IncludeModule($module_id);

	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
	IncludeModuleLangFile(__FILE__);

	$arProducts = ['' => ''];
	foreach (CRunetPay::Products() as $product)
	{
		if ($product->Manager === 'EventProductManager')
		{
			$arProducts[$product->ProductId] = $product->Title;
		}
	}

	$arAlloptions = [[[
		'title' => GetMessage('RUNET.OPTION.APIKEY'),
		'name' => 'apikey',
		'type' => 'text',
		'value' => Option::get($module_id, 'apikey')
	],[
		'title' => GetMessage('RUNET.OPTION.SECRET_KEY'),
		'name' => 'secretkey',
		'type' => 'text',
		'value' => Option::get($module_id, 'secretkey')
	],[
		'title' => GetMessage('RUNET.OPTION.USE_BITRIX_GROUPS'),
		'name' => 'useBxGroups',
		'type' => 'checkbox',
		'value' => Option::get($module_id, 'useBxGroups')
	],[
		'title' => GetMessage('RUNET.OPTION.DEFAULT_ROLEID'),
		'name' => 'defaultRoleId',
		'type' => 'selectbox',
		'value' => Option::get($module_id, 'defaultRoleId'),
		'values' => CRunetEvent::GetRoles()
	],[
		'title' => GetMessage('RUNET.OPTION.DEFAULT_PRODUCTID'),
		'name' => 'defaultProductId',
		'type' => 'selectbox',
		'value' => Option::get($module_id, 'defaultProductId'),
		'values' => $arProducts
	],[
		'title' => GetMessage('RUNET.OPTION.DATA_SYNC'),
		'name' => 'dataSync',
		'type' => 'checkbox',
		'value' => Option::get($module_id, 'dataSync')
	],[
		'title' => GetMessage('RUNET.OPTION.PREVENT_LOCAL_LOGIN'),
		'name' => 'preventSystemLogin',
		'type' => 'checkbox',
		'value' => Option::get($module_id, 'preventSystemLogin')
	],[
		'title' => GetMessage('RUNET.OPTION.ADDITIONAL_MAIL_CHECK'),
		'name' => 'useAdditionalEmailCheck',
		'type' => 'checkbox',
		'value' => Option::get($module_id, 'useAdditionalEmailCheck')
	]]];

	$aTabs = [
		['DIV' => 'settings', 'TAB' => GetMessage('RUNET.OPTION.TAB.SETTINGS'), 'TITLE' => GetMessage('RUNET.OPTION.TAB.SETTINGS_TITLE')],
		['DIV' => 'rights', 'TAB' => GetMessage('MAIN_TAB_RIGHTS'), 'TITLE' => GetMessage("MAIN_TAB_TITLE_RIGHTS")]
	];

	// Сохранение настоек
	if ($REQUEST_METHOD == 'POST' && check_bitrix_sessid())
	{
		// Импорт ролей с RunetId
		if (!empty($_REQUEST['useBxGroups']))
			CRunetRoleGroupLink::Instance()->Update();

		foreach ($arAlloptions as $options)
		{
			foreach ($options as &$option)
			{
				$value = $_REQUEST[$option['name']];
				switch ($option['type'])
				{
					case 'text':
					case 'selectbox':
						Option::set($module_id, $option['name'], $value);
						break;

					case 'checkbox':
						Option::set($module_id, $option['name'], empty($value) ? 'N' : 'Y');
						break;
				}
			}
		}

		LocalRedirect($action_url);
	}

	$tabControl = new CAdminTabControl('tabControl', $aTabs);
	$tabControl->Begin();

?>
<form method="POST" action="<?=$action_url?>">
	<?=bitrix_sessid_post()?>
	<?foreach ($arAlloptions as $options):?>
		<?php

			$tabControl->BeginNextTab();

			foreach ($options as $option)
			{
				echo '<tr>';
				echo '<td valign="top" width="50%">'.$option['title'].':</td>';
				echo '<td valign="top" width="50%">';
				switch ($option['type'])
				{
					case 'text':
						echo '<input type="text" name="'.$option['name'].'" value="'.$option['value'].'" />';
						break;

					case 'selectbox':
						echo '<select name="'.$option['name'].'" '.($option['multiple'] ? 'multiple="multiple"' : '').'>';
						foreach ($option['values'] as $value => $title)
							echo '<option value="'.$value.'" '.($value == $option['value'] ? 'selected="selected"' : '').'>'.$title.'</option>';
						echo '</select>';
						break;

					case 'checkbox':
						echo '<input type="checkbox" name="'.$option['name'].'" value="'.$option['value'].'" '.($option['value'] === 'Y' ? 'checked="checked"' : '').'/>';
						break;

				}
				echo '</td>';
				echo '</tr>';
			}
		?>
	<?endforeach?>
	<?php
		$tabControl->BeginNextTab();
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
		$tabControl->Buttons();
	?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?=GetMessage("MAIN_RESET")?>">
	<?$tabControl->End()?>
</form>
