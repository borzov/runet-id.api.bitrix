<?php

	IncludeModuleLangFile(__FILE__);

	class runet extends CModule
	{
		public $MODULE_ID = 'runet';
		public $MODULE_VERSION = '';
		public $MODULE_VERSION_DATE = '';
		public $MODULE_NAME = '';
		public $MODULE_DESCRIPTION = '';
		public $MODULE_CSS;
		public $MODULE_GROUP_RIGHTS = 'Y';

		public function __construct()
		{
			/** @var string $arModuleVersion */

			include str_replace(['\\', 'index.php'], ['/', 'version.php'], __FILE__);

			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

			$this->MODULE_NAME = GetMessage('RUNET.MODULE.NAME');
			$this->MODULE_DESCRIPTION = GetMessage('RUNET.MODULE.DESCRIPTION');
		}

		public function DoInstall()
		{
			RegisterModuleDependences('main', 'OnProlog', $this->MODULE_ID, 'CRunetModule', 'OnProlog');
			RegisterModuleDependences('main', 'OnBeforeUserAdd', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserAdd');
			RegisterModuleDependences('main', 'OnBeforeUserUpdate', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserUpdate');
			RegisterModuleDependences('main', 'OnBeforeUserLogin', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserLogin');
			RegisterModuleDependences('main', 'OnAfterUserLogin', $this->MODULE_ID, 'CRunetModule', 'OnAfterUserLogin');

			$userTypeEntity = new CUserTypeEntity();
			$userTypeEntity->Add([
				'ENTITY_ID' => 'USER',
				'FIELD_NAME' => 'UF_RUNET',
				'USER_TYPE_ID' => 'integer'
			]);

			/* Создаём необходимые обработчики почтовых событий */
			$obEventType = new CEventType();
			$obEventMessage = new CEventMessage();
			$obLang = CLanguage::GetList($b = "", $o = "");

			while ($arLang = $obLang->Fetch())
			{
				$obEventType->Add([
					'LID' => $arLang['ID'],
					'EVENT_NAME' => 'RUNET_CHECK_EMAIL',
					'NAME' => GetMessage('RUNET.EVENT.CHECK.EMAIL'),
					'DESCRIPTION' => ''
						. "#EMAIL# - sdfasfdfdafd\n"
						. "#FIO# - Фамилия, Имя и Отчество\n"
						. "#URL# - Ссылка для проверки E-Mail\n",
					'SORT' => 10
				]);

				$obEventMessage->Add([
					'EVENT_NAME' => 'RUNET_CHECK_EMAIL',
					'LID' => 's1',
					'BODY_TYPE' => 'html',
					'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
					'EMAIL_TO' => '#EMAIL#',
					'SUBJECT' => GetMessage('RUNET.EVENT.CHECK.EMAIL.MESSAGE'),
					'MESSAGE' => GetMessage('RUNET.EVENT.CHECK.EMAIL.MESSAGE.DESCR')
				]);
			}

			RegisterModule($this->MODULE_ID);
			$this->InstallFiles();
		}

		public function DoUninstall()
		{
			UnRegisterModuleDependences('main', 'OnProlog', $this->MODULE_ID, 'CRunetModule', 'OnProlog');
			UnRegisterModuleDependences('main', 'OnBeforeUserAdd', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserAdd');
			UnRegisterModuleDependences('main', 'OnBeforeUserUpdate', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserUpdate');
			UnRegisterModuleDependences('main', 'OnBeforeUserLogin', $this->MODULE_ID, 'CRunetModule', 'OnBeforeUserLogin');
			UnRegisterModuleDependences('main', 'OnAfterUserLogin', $this->MODULE_ID, 'CRunetModule', 'OnAfterUserLogin');

			CEventType::Delete('RUNET_CHECK_EMAIL');

			$by = '';
			$order = '';
			$obEventMessage = CEventMessage::GetList($by, $order, ['EVENT_NAME' => 'RUNET_CHECK_EMAIL']);
			while ($arEventMessage = $obEventMessage->Fetch())
				CEventMessage::Delete($arEventMessage['ID']);

			UnRegisterModule($this->MODULE_ID);
		}

		public function GetModuleRightList()
		{
			$rights = [
				'reference_id' => ['D', 'R', 'W'],
				'reference' => [
					'[D] '.GetMessage('RUNET.RIGTH.DENIED'),
					'[R] '.GetMessage('RUNET.RIGTH.OPENED'),
					'[W] '.GetMessage('RUNET.RIGTH.FULL')
				]
			];

			return $rights;
		}

		public function InstallFiles()
		{
			CopyDirFiles(
				$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/runet/install/components',
				$_SERVER['DOCUMENT_ROOT'].'/bitrix/components',
				true, true
			);
		}
	}
