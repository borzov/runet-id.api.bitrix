<?php
CModule::AddAutoloadClasses(
  "runet",
  array(
  	'CRunetModule' => 'classes/general/module.php',
  	'CRunetGate' => 'classes/general/runet_gate.php',
  	'CRunetGateUser' => 'classes/general/runet_gate_user.php',
  	'CRunetUser' => 'classes/general/runet_user.php',
  	'CRunetException' => 'classes/general/runet_exception.php',
  	'CRunetEvent' => 'classes/general/runet_event.php',
  	'CRunetPay' => 'classes/general/runet_pay.php',
  	'CRunetRoleGroupLink' => 'classes/general/runet_role_group_link.php',
  	'CRunetOrderList' => 'classes/general/runet_order_list.php',
    'CRunetCompany' => 'classes/general/runet_company.php',
    'CRunetInvite' => 'classes/general/runet_invite.php',
    'CRunetPurpose' => 'classes/general/runet_purpose.php',
    'CRunetProfessionalInterest' => 'classes/general/runet_professional_interest.php',
      'CrunetIRI' => 'classes/general/runet_iri.php'
/*
//    'CRocidGate' => 'classes/general/rocid_gate.php',
//    'CRocidGateUser' => 'classes/general/rocid_gate_user.php',
//    'CRocidException' => 'classes/general/rocid_exception.php',
//    'CRocidUser' => 'classes/general/rocid_user.php',
//    'CRocidEvent' => 'classes/general/rocid_event.php',
//    'CRocidPay' => 'classes/general/rocid_pay.php',
//    'CRocidRoleGroupLink' => 'classes/general/rocid_role_group_link.php',
//		'CRocidOrderList' => 'classes/general/rocid_order_list.php',
//		'CRocidCompany' => 'classes/general/rocid_company.php',

    'CRocidUtility' => 'classes/general/rocid_utility.php',
    'CRocidGeo' => 'classes/general/rocid_geo.php',
    'CRocidRAEC' => 'classes/general/rocid_raec.php'
*/
  )
);
?>
