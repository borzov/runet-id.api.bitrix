<?php
class CRunetProfessionalInterest
{
  /**
   * Добавляет пользователю проф. интерес
   *
   * @param int $runetId
   * @param int $professionalInterestId
   * @return array
   */
  public static function Add($runetId, $professionalInterestId)
  {
    $result = CRunetGate::Instance()->Post('/professionalinterest/add', array(
      'RunetId' => $runetId,
      'ProfessionalInterestId' => $professionalInterestId
    ));

    if ( isset($result->Error) && $result->Error === true)
    {
      //throw new CRunetException($result->Error->Message);
    }
    return $result;
  }

  /**
   * Удаляет у пользователя проф. интерес
   *
   * @param int $runetId
   * @param int $professionalInterestId
   * @return array
   */
  public static function Delete($runetId, $professionalInterestId)
  {
    $result = CRunetGate::Instance()->Post('/professionalinterest/delete', array(
      'RunetId' => $runetId,
      'ProfessionalInterestId' => $professionalInterestId
    ));

    if ( isset($result->Error) && $result->Error === true)
    {
      throw new CRunetException($result->Error->Message);
    }
    return $result;
  }

  /**
   * Возвращает список проф. интересов
   * @return array
   */
  public static function GetList()
  {
    $result = CRunetGate::Instance()->Get('/professionalinterest/list', array(), 3600);
    return $result;
  }
}