<?php
class CRunetRoleGroupLink
{
	private $runetRoles = array();
	private $links = array();
	protected static $instance = null;
    
  private function __construct()
  {
  	$this->runetRoles = CRunetEvent::GetRoles();
    $this->links = unserialize(COption::GetOptionString('runet', 'roleGroupLink'));
  }
    
  private function __clone()     {}
  private function __wakeup()    {}
    
  public static function Instance ()
  {
  	if (self::$instance === null)
    {
    	self::$instance = new CRunetRoleGroupLink();
    }
    return self::$instance;
  }
    
  /**
   *
   * @param  string $roleName
   * @return string 
   */
  private static function GetGroupName ($roleName)
  {
  	return 'runet_'.$roleName;
  }

  /**
   *
   * @param  string $roleName
   * @return boolean
   */
  public function CheckGroup ($roleName) 
  {
  	$bxGroup = CGroup::GetList($_,$_, array('NAME' => self::GetGroupName($roleName)))->Fetch();
    return (!empty($bxGroup));
  }
    
	/**
	 *
	 * @param  int $roleId
	 * @return int 
	 */
	public function GetGroupId ($roleId)
	{
		if ( isset ($this->links[$roleId]))
		{
			return $this->links[$roleId];
		}
		return 0;
	}
    
	/**
	*
	* @param  int $roleId
	* @param  string $roleName
	* @return boolean 
	*/
	public function AddGroup ($roleId, $roleName)
	{
		if ( !$this->CheckGroup($roleName))
		{
			$bxGroupObj = new CGroup;
			$newGroupId = $bxGroupObj->Add(array(
				'NAME' => self::GetGroupName($roleName),
				'ACTIVE' => 'Y'
			));
			if ( $newGroupId > 0)
			{
				$this->AddLink($roleId, $newGroupId);
				return true;
			}
		}
	  return false;
	}
    
	/**
	 *
	 * @param int $roleId
	 * @param int $groupId 
	 */
	private function AddLink ($roleId, $groupId)
	{
		$this->links[$roleId] = $groupId;
		COption::SetOptionString(
			'runet', 'roleGroupLink', serialize($this->links)
		);
	}
  
	/**
	 * 
	 */
	public function Update ()
	{
		$runetRoles = $this->runetRoles;
		foreach ($runetRoles as $roleId => $roleName) 
		{
			$this->AddGroup($roleId, $roleName);
		}
	}
    
	/**
	*
	* @return boolean 
	*/
	public static function IsActive()
	{
		$useBxGroups = COption::GetOptionString('runet', 'useBxGroups');
		if ($useBxGroups === 'Y')
		{
			return true;
		}
		return false;
	}
    
	/**
	*
	* @param int $userId
	* @param int $roleId 
	*/
  public function SetUserRole ($userId, $roleId)
  {
    $bxUserObj = new CUser;
    $groups = CUser::GetUserGroup($userId);
    $changed = false;
    $newGroup = $this->GetGroupId($roleId);
    foreach ($groups as $k => $group)
    {
      if (in_array($group, $this->links) && $newGroup != $group)
      {
        unset($groups[$k]);
        $changed = true;
      }
    }

    if (! in_array($newGroup, $groups))
    {
      $groups[] = $newGroup;
      $changed = true;
    }

    if ($changed)
    {
      $bxUserObj->Update($userId, array('GROUP_ID' => $groups));
    }
    $bxUserObj->Authorize($userId);
  }
}
?>
