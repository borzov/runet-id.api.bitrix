<?php
class CRunetEvent 
{
	/**
	 *
	 * @return array 
	 */
	public static function GetRoles ()
	{
	  $roles = CRunetGate::Instance()->Get('event/role/list', null, 604800);
	  $result = array();
	  foreach ($roles as $role) 
	  {
	  	$result[$role->RoleId] = $role->Name;
	  }
	  return $result;
	}

	/**
	 *
	 * @return int 
	 */
	public static function GetDefaultRoleId ()
	{
		return (int) COption::GetOptionString('runet', 'defaultRoleId');
	}
    
	/**
	* Изменение роли на мероприятии
	* 
	* @param int $RunetId
	* @param int $RoleId
	* @return bool
	*/
	public static function ChangeRole ($RunetId, $RoleId)
	{
	    $result = CRunetGate::Instance()->Post('event/changerole', array(
	        'RunetId' => $RunetId,
	        'RoleId' => $RoleId
	    ));
	    return $result->Success;
	}

	/**
	 * РЕГИСТРАЦИЯ НА МЕРОПРИЯТИЕ
	 * 
	 * @param  int $runetId
	 * @param  int $roleId
	 * @param  bool $$usePriority - использовать системные приоритеты статусов
	 * @return bool  
	 */
	public static function Register($runetId, $roleId = null, $usePriority = true)
	{
		if ($roleId === null)
		{
			$roleId = self::GetDefaultRoleId();
		}
		$result = CRunetGate::Instance()->Post('event/register', array(
			'RunetId'  => $runetId,
			'RoleId' => $roleId,
			'UsePriority' => $usePriority
		));
		
		if ($result->Success === true)
		{
			$defaultProductId = CRunetPay::GetDefaultProductId();
			if ($defaultProductId > 0)
			{
				CRunetPay::Add($defaultProductId, $runetId);
			}
			$events = GetModuleEvents('runet', 'onRegister');
			while ($event = $events->Fetch()) 
			{
				ExecuteModuleEvent($event, $runetId);
			}
			return true;
		}
		else
		{
			return false;
		}
	}
    
	/**
	* СПИСОК УЧАСТНИКОВ МЕРОПРИЯТИЯ
	*
	* @param string $query
	* @param int/array    $roleId
	* @param int    $maxResult
	* @return CRunetUserData[] 
	*/
	public static function Users ($query = '', $roleId = array(), $maxResult = 200, $sortBy = 'LastName', $cache = 3600)
	{
	  $result = array();
	  $pageToken = null;
	   
	  do {
	    $gateResult = CRunetGate::Instance()->Get('event/users', array(
	      'Query'      => $query,
	      'RoleId'     => $roleId,
	      'MaxResults' => $maxResult,
	      'PageToken'  => $pageToken,
	      'SortBy'     => $sortBy
	    ), $cache);

	    $pageToken = $gateResult->NextPageToken;

	    $result = array_merge(
	      $result, $gateResult->Users
	    );
	  } 
	  while ($pageToken !== null);
	  return $result;
	}
   
	/**
	*
	* @param string $query
	* @param int    $roleId
	* @param int    $maxResult
	* @return CRunetUserData[] 
	*/
	public function OnlyUpdateUsers ($updateTime = '', $query = '', $roleId = null)
	{
	    $result = array();

	    $gateResult = CRunetGate::Instance()->Get('event/users/updated', array(
	        'Query'      => $query,
	        'RoleId'     => $roleId,
	        'FromUpdateTime' => $updateTime
	    ), 3600);
	    if ($gateResult->Error == 666)
	    {
	        $result = $this->Users($query, $roleId);
	    }
	    else
	    {
	        $result = $gateResult->Users;
	    }

	    return $result;
	}
   
	/**
	* Получение информации по секции программы мероприятия
	* 
	* @param int $sectionId
	* @return object
	*/
	public static function GetSection($sectionId)
	{
	  $result = CRunetGate::Instance()->Post('event/section/info', array(
	    'SectionId' => $sectionId,
	  ), 600);

	  if (isset($result->Error) && $result->Error === true)
	  {
	      return false;
	  }
	  else
	  {
	      return $result;
	  }
	}
   
	/**
	* Получение списка секций программы
	* 
	* @return object
	*/
	public static function GetSectionList($filter = array())
	{
	  $result = CRunetGate::Instance()->Post('event/section/list', array('Filter' => $filter), 600);

	  if (isset($result->Error) && $result->Error === true)
	  {
	      return false;
	  }
	  else
	  {
	      return $result;
	  }
	}
   
	/**
	* Получение списка докладчиков/ведущих в секции
	* 
	* @param int $sectionId
	* @return array
	*/
	public static function GetSectionReports($sectionId)
	{
	  $result = CRunetGate::Instance()->Post('event/section/reports', array(
	      'SectionId' => $sectionId,
	  ), 600);
	
	  if (isset($result->Error) 
	          && $result->Error === true)
	  {
	      return false;
	  }
	  else
	  {
	      return $result;
	  }
	}
   
	/**
	* Получение списка секций по RunetId докладчика
	* 
	* @param int $runetId
	* @return array
	*/
	public static function GetUserSections($runetId)
	{
	  $result = CRunetGate::Instance()->Post('event/section/user', array(
	      'RunetId' => $runetId,
	  ), 600);

	  if (count($result) == 0)
	  {
	      return false;
	  }
	  else
	  {
	      return $result;
	  }
	}
   
	/**
	*
	* @param  int $runetId
	* @param  string[] $events
	* @return boolean 
	*/
	public static function Check ($runetId, $events) 
	{
		 $result = CRunetGate::Instance()->Get('event/check', array(
		  'Events' => $events,
		  'RunetId'  => $runetId
		 ));
		 
		 if ( isset($result->Error)
		         && $result->Error === true)
		 {
		     return false;
		 }
		 return $result->Check;
	}

  /**
   * ТОП-100 компаний-участниц мероприятия
   * 
   * @static
   * @return array
   */
  public static function GetTopCompanies()
  {
    $result = CRunetGate::Instance()->Post('/event/companies/', array(), 3600);
    return $result;
  }

  /**
   * Кол-во зарегистрированных по статусам
   * 
   * @static
   * @return array
   */
  public static function GetStatistics()
  {
    $result = CRunetGate::Instance()->Post('/event/statistics/', array(), 600);
    return $result;
  }

  /**
   * Список доступных целей для мероприятия
   *
   * @return array
   */
  public static function GetPurposes()
  {
    $result = CRunetGate::Instance()->Get('/event/purposes', array(), 3600);
    return $result;
  }
}
?>
