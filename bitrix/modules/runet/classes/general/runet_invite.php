<?php
class CRunetInvite
{
  /**
   * @param int $RunetId
   * @return \stdClass
   */
  public static function Request($RunetId)
  {
    $result = CRunetGate::Instance()->Post('invite/request', array(
      'RunetId' => $RunetId,
    ));
    return $result->Success;
  }

  /**
   * @param $RunetId
   * @return stdClass|null
   */
  public static function Get($RunetId)
  {
    $result = CRunetGate::Instance()->get('invite/get', array(
      'RunetId' => $RunetId,
    ));

    if (isset($result->Error))
    {
      return null;
    }
    else
    {
      return $result;
    }
  }
} 