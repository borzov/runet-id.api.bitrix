<?php
class CRunetIRI
{
    public static function GetRoles ()
    {
        $roles = CRunetGate::Instance()->Get('iri/roles');

        $result = array();
        foreach ($roles as $role)
        {
            $result[$role->RoleId] = $role->Name;
        }
        return $result;
    }

    public static function GetParticipants ()
    {
        return CRunetGate::Instance()->Get('iri/users', null, 604800);
    }

    public static function JoinIRI($RunetId, $RoleId)
    {
        $result = CRunetGate::Instance()->Post('iri/userjoin', array(
            'RunetId' => $RunetId,
            'RoleId' => $RoleId
        ));
        return $result->Success;
    }

    public static function exitIRI($RunetId, $RoleId)
    {
        $result = CRunetGate::Instance()->Post('iri/userexit', array(
            'RunetId' => $RunetId,
            'RoleId' => $RoleId
        ));
        return $result->Success;
    }
}
