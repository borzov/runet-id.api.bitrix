<?php
class CRunetPurpose
{
  /**
   * Добавляет цель посещения пользователю
   *
   * @param int $runetId
   * @param int $purposeId
   * @return array
   * @throws CRunetException
   */
  public static function Add($runetId, $purposeId)
  {
    $result = CRunetGate::Instance()->Post('/purpose/add', array(
      'RunetId' => $runetId,
      'PurposeId' => $purposeId
    ));

    if ( isset($result->Error) && $result->Error === true)
    {
      throw new CRunetException($result->Error->Message);
    }
    return $result;
  }

  /**
   * Удаляет цель посещения у пользователя
   *
   * @param int $runetId
   * @param int $purposeId
   * @return array
   * @throws CRunetException
   */
  public static function Delete($runetId, $purposeId)
  {
    $result = CRunetGate::Instance()->Post('/purpose/delete', array(
      'RunetId' => $runetId,
      'PurposeId' => $purposeId
    ));

    if ( isset($result->Error) && $result->Error === true)
    {
      throw new CRunetException($result->Error->Message);
    }
    return $result;
  }
} 