<?php
class CRunetOrderList
{
	private $orderList = null;
	private $userList = null;
	private $invoice = false;
	private $amount = 0;
	private $runetId;
	
	public function __construct($runetId)
	{
		$this->runetId = $runetId;
		$this->orderList = CRunetPay::GetOrderList($runetId);
	}
	
	/**
	* Удаление товаров пользователя
	* 
	* @param int $ownerRunetId
	* @param string $productManager
	*/
	public function DeleteUser($ownerRunetId, $productManager = false)
	{
		$deletedOrderItemId = false;
		
		$this->GetUserList();
		
		if ($this->userList !== null)
		{
			if (isset($this->userList[$ownerRunetId]))
			{
				foreach($this->userList[$ownerRunetId] as $item)
				{
					if ($item->Paid === false)
					{
						if ($productManager !== false)
						{
							if ($item->Product->Manager == $productManager)
							{
								CRunetPay::Delete($item->OrderItemId, $this->runetId);
								$deletedOrderItemId[] = $item->OrderItemId;
							}
						}
						else
						{
							CRunetPay::Delete($item->OrderItemId, $this->runetId);
							$deletedOrderItemId[] = $item->OrderItemId;
						}
					}
				}
			}
		}
		return $deletedOrderItemId;
	}
	
	/**
	* Удаление товара из заказа
	* 
	* @param int $ownerRunetId
	* @param int $productId
	*/
	public function DeleteProduct($ownerRunetId, $productId = false)
	{
		$this->GetUserList();
		
		if ($this->userList !== null)
		{
			if (isset($this->userList[$ownerRunetId]))
			{
				foreach($this->userList[$ownerRunetId] as $item)
				{
					if ($item->Paid === false)
					{
						if ($productId !== false)
						{
							if ($item->Product->ProductId == $productId)
							{
								CRunetPay::Delete($item->OrderItemId, $this->runetId);
							}
						}
						else
						{
							CRunetPay::Delete($item->OrderItemId, $this->runetId);
						}
					}
				}
			}
		}
	}
	
	/**
	* Переформированный список пользователей/товаров в заказе
	* @param bool $combine - комбинировать оплаченных и неоплаченных
	* @param bool $paid - только оплаченных (при @combine = false)
	* 
	*/
	public function GetUserList($combine = false, $paid = false)
	{

		$this->userList = null;

		if ($this->orderList !== null)
		{
			if (count($this->orderList->Items) > 0)
			{
				foreach($this->orderList->Items as $item)
				{
					$item->Invoice = false;
					if ($combine)
          {
            $this->userList[$item->Owner->RunetId][$item->Id] = $item;
          }
          else
          {
            if ($item->Paid === $paid)
					  {
						  $this->userList[$item->Owner->RunetId][$item->Id] = $item;
					  }
          }
				}
			}

			if (count($this->orderList->Orders) > 0)
			{
				$this->invoice = true;
				foreach($this->orderList->Orders as $Order)
				{
					if ($Order->Paid)
					{
						continue;
					}
					foreach($Order->Items as $item)
					{
						$item->Invoice = $Order->Url;
            if ($combine)
            {
              $this->userList[$item->Owner->RunetId][$item->Id] = $item;
            }
            else
            {
						  if ($item->Paid === $paid)
						  {
							  $this->userList[$item->Owner->RunetId][$item->Id] = $item;
						  }
            }
					}
				}
			}
		}
		
		return $this->userList;

	}
	
	
	/**
	* Есть ли товар в списке заказов
	* 
	* @param int $productId
	* @param int $ownerRunetId
	* @param bool $paid - если null, то параметр не учитывается
  * @return bool
	*/
	public function IsProductInOrder($productId, $ownerRunetId, $paid = null)
	{
		$this->GetUserList(true);

		if ($this->userList !== null)
		{
			if (isset($this->userList[$ownerRunetId]))
			{
				foreach($this->userList[$ownerRunetId] as $item)
				{
          if ($paid === null)
          {
            if ($item->Product->ProductId == $productId)
            {
              return true;
              break;
            }
          }
          else
          {
            if ($item->Product->ProductId == $productId && $item->Paid === $paid)
            {
              return true;
              break;
            }
          }
				}
			}
		}
		return false;
	}
	
	/**
	* ЕСТЬ ЛИ СКИДКА НА ТОВАР У ПОЛЬЗОВАТЕЛЯ
	* 
	* @param int $productId
	* @param int $ownerRunetId
	*/
	public function IsDiscount($productId, $ownerRunetId)
	{
		$this->GetUserList(true);

		if ($this->userList !== null)
		{
			if (isset($this->userList[$ownerRunetId]))
			{
				foreach($this->userList[$ownerRunetId] as $item)
				{
					if ($item->Product->Id == $productId && $item->Discount != 0)
					{
						return array('PriceDiscount' => $item->PriceDiscount, 'Discount' => $item->Discount);
						break;
					}
				}
			}
		}
		return false;
	}
	
	/**
	* Кол-во товаров без купонов
	* 
	* @param int $productId
	* @param str $ownerRunetId
	*/
	public function GetWholesaleUsers($productId, $couponCode)
	{
		$this->GetUserList();
		
		$out = array();

		if ($this->userList !== null)
		{
			foreach($this->userList as $runetId => $items)
			{
				foreach($items as $item)
				{
					if ($item->Product->ProductId == $productId && ($item->Discount == 0 || ($couponCode != '' && $item->CouponCode == $couponCode)))
					{
						$out[$runetId]['wholesale'] = ($item->CouponCode != '');
					}
				}
			}
		}
		return $out;
	}
	
	/**
	* Выставлен ли счёт на юр. лицо
	* 
	*/
	public function IsInvoice()
	{
		if ($this->invoice) return true;
		if ($this->orderList !== null)
		{
			if (count($this->orderList->Orders) > 0)
			{
				$this->invoice = true;
				return true;
			}
		}
		return false;
	}
	
	/**
	* Оплачены ли ВСЕ заказы пользователя
	* 
	*/
	public function IsAllPaid()
	{
		$this->GetUserList();
		
		if ($this->userList !== null)
		{
			foreach($this->userList as $runetId => $userItems)
			{
				foreach($userItems as $item)
				{
					if ($item->Paid === false)
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	* Посчитать сумму неоплаченных заказов
	* 
	* @param string $productManager
	*/
  private function FillAmount($productManager, $runetId = false)
  {
    // Если получен runetID, то паразагрузить список товаров
    if ($runetId !== false) {
      $this->orderList = CRunetPay::GetOrderList($runetId);
      $this->amount = 0;
    }
    if ($this->orderList !== null)
    {
      if (count($this->orderList->Items) > 0)
      {
        foreach($this->orderList->Items as $item)
        {
          if ($item->Paid === false && !$item->Deleted)
          {
            if ($productManager !== false)
            {
              if ($item->Product->Manager == $productManager)
              {
                $this->amount += $item->PriceDiscount;
              }
            }
            else
            {
              $this->amount += $item->PriceDiscount;
            }
          }
        }
      }

      if (count($this->orderList->Orders) > 0)
      {
        $this->invoice = true;
        foreach($this->orderList->Orders as $Order)
        {
          if ($Order->Paid)
          {
            continue;
          }
          foreach($Order->Items as $item)
          {
            if ($item->Paid === false && !$item->Deleted)
            {
              if ($productManager !== false)
              {
                if ($item->Product->Manager == $productManager)
                {
                  $this->amount += $item->PriceDiscount;
                }
              }
              else
              {
                $this->amount += $item->PriceDiscount;
              }

            }
          }
        }
      }
    }
  }
	
	public function GetAmount($productManager = false, $runetId = false)
	{
		$this->FillAmount($productManager, $runetId);
		return $this->amount;
	}
	
	/**
	* ФИГУРИРУЕТ ЛИ ПОЛЬЗОВАТЕЛЬ В ЗАКАЗАХ ОСТАЛЬНЫХ УЧАСТНИКОВ МЕРОПРИЯТИЯ
	* 
	* @param int $runetId
	*/
	public function IsUserInOtherOrders($runetId)
	{
		$userItems = CRunetPay::GetOrderItems($runetId);
		if ($userItems !== null)
		{
			foreach($userItems as $Item)
			{
				if ($Item->Owner->RunetId == $runetId && $Item->Payer->RunetId != $Item->Owner->RunetId)
				{
					return $Item;
				}
			}
		}
		return false;
	}
	
	public function CouponDelete($runetId, $productId, $code = '')
	{
		$this->GetUserList(true);
		
		if ($code == '')
		{
			if ($this->userList !== null)
			{
				foreach($this->userList[$runetId] as $item)
				{
					if ($item->Product->ProductId == $productId && $item->Discount != 0 && $item->CouponCode != '')
					{
						$code = $item->CouponCode;
					}
				}
			}
		}
		return CRunetPay::CouponDelete($code, $runetId);
	}
	
	/**
	* УДАЛЕНИЕ ЗАКАЗОВ
	* 
	* @param int $payerRunetId
	* @param string $productManager
	*/
	public function ClearOrder($payerRunetId, $productManager = false)
	{
		$this->GetUserList();

		if ($this->userList !== null)
		{
			foreach($this->userList as $user)
			{
				foreach($user as $item)
				{
					if ($item->Paid === false)
					{
						if ($productManager !== false)
						{
							if ($item->Product->Manager == $productManager)
							{
								$result = CRunetPay::Delete($item->OrderItemId, $payerRunetId);
							}
						}
						else
						{
							CRocidPay::Delete($item->OrderItemId, $ownerRunetId);
						}
					}
				}
			}
		}
	}

	
}