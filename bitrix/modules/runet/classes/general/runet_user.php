<?php

	use Bitrix\Main\Config\Option;

	class CRunetUser
	{
		private $user;
		private $runetUser;

		/**
		 *
		 * @param CUser $user
		 */
		protected function __construct($user)
		{
			$this->user = $user;

			if ($this->user->IsAuthorized())
			{
				$bxUser = CUser::GetList($_, $_, ['ID' => $this->user->GetID()], ['SELECT' => ['UF_RUNET']])->Fetch();

				if ($bxUser['UF_RUNET'] !== 0)
					$this->runetUser = CRunetGateUser::Get($bxUser['UF_RUNET']);

				if (CRunetRoleGroupLink::IsActive())
				{
					$roleId = ($this->GetRoleId() > 0)
						? $this->GetRoleId()
						: CRunetEvent::GetDefaultRoleId();

					CRunetRoleGroupLink::Instance()->SetUserRole($this->user->GetID(), $roleId);
				}
			}
		}

		protected function __clone()
		{
		}

		protected function __wakeup()
		{
		}

		public function __call($name, $args)
		{
			if (method_exists($this->user, $name))
				return $this->user->$name($args);

			return null;
		}

		private static $instance = null;

		public static function Instance()
		{
			if (self::$instance === null)
				self::$instance = new CRunetUser($GLOBALS['USER']);

			return self::$instance;
		}

		/**
		 * ПОЛУЧЕНИЕ ЛОГИНА ПОЛЬЗОВАТЕЛЯ BITRIX
		 *
		 * @param  int $runetId
		 * @return string
		 */
		private static function GetLoginField($runetId)
		{
			return 'runetId_'.$runetId;
		}

		/**
		 * АВТОРИЗАЦИЯ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @param  string $runetIdOrEmailOrToken
		 * @param  string $password
		 * @return boolean
		 */
		public function Login($runetIdOrEmailOrToken, $isToken = false)
		{
			if ($this->IsAuthorized())
				return true;

			$user = (!$isToken) ? CRunetGateUser::Get($runetIdOrEmailOrToken) : CRunetGateUser::GetByToken($runetIdOrEmailOrToken);

			if (!$user)
				return false;

			$arBitrixUser = CUser::GetList($_, $_, ['LOGIN_EQUAL' => self::GetLoginField($user->RunetId)])->Fetch();

			if (empty($arBitrixUser))
			{
				$fields = [
					'ACTIVE' => Option::get('runet', 'useAdditionalEmailCheck', 'N') === 'Y' ? 'N' : 'Y',
					'LOGIN' => self::GetLoginField($user->RunetId),
					'NAME' => $user->FirstName,
					'LAST_NAME' => $user->LastName,
					'EMAIL' => empty($user->Email)
						? self::GetLoginField($user->RunetId).'@runet-id.com'
						: $user->Email,
					'GROUP_ID' => explode(',', Option::get('main', 'new_user_registration_def_group')),
					'PASSWORD' => ($password = md5(microtime())),
					'CONFIRM_PASSWORD' => $password,
					'UF_RUNET' => $user->RunetId
				];

				if (isset($user->Work))
				{
					$fields['WORK_COMPANY'] = $user->Work->Company->Name;
					$fields['WORK_POSITION'] = $user->Work->Position;
				}

				$arBitrixUser['ID'] = $this->user->Add($fields);

				if ($arBitrixUser['ID'] === false)
				{
					echo $this->user->LAST_ERROR;
					exit;
				}

				else
				{
					CEvent::Send('NEW_USER', ['s1'], $fields);
				}
			}
			/* Запускаем обработчик события авторизации пользователя на сайте,
			 * сообщая ему о том, что этот запуск именно от RunetID. */
			if (!CRunetModule::OnBeforeUserLogin($arBitrixUser, true))
				return false;

			$authResult = $this->user->Authorize($arBitrixUser['ID'], true);
			/*

						print 'User:<pre style="font-size: 14px;">';
						var_dump($user);
						print '</pre>';
						print 'BXUser:<pre style="font-size: 14px;">';
						var_dump($arBitrixUser);
						print '</pre>';
						print 'BXAuth:<pre style="font-size: 14px;">';
						var_dump($authResult);
						print '</pre>';
						exit();
			*/

			if ($authResult)
			{
				$this->runetUser = $user;
				$arCredentials = [
					'USER_ID' => $arBitrixUser['ID'],
					'LOGIN' => $arBitrixUser['LOGIN'],
					'PASSWORD' => $arBitrixUser['PASSWORD'],
					'REMEMBER' => 'N',
					'PASSWORD_ORIGINAL' => 'N'
				];

				CRunetModule::OnAfterUserLogin($arCredentials);

				return true;
			}

			return false;
		}

		/**
		 * ПЕРЕЗАГРУЗКА ДАННЫХ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return boolean
		 */
		public function Reload()
		{
			if ($this->runetUser === null)
				return false;

			$this->runetUser = CRunetGateUser::Get($this->GetRunetId(), 30, true);

			if (CRunetRoleGroupLink::IsActive() && $this->GetRoleId() > 0)
				CRunetRoleGroupLink::Instance()->SetUserRole($this->user->GetID(), $this->GetRoleId());

			return true;
		}

		/**
		 * RunetID ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return int
		 */
		public function GetRunetId()
		{
			return $this->runetUser->RunetId;
		}

		/**
		 * ИМЯ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return string
		 */
		public function GetFirstName()
		{
			return $this->runetUser->FirstName;
		}

		/**
		 * ФАМИЛИЯ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return string
		 */
		public function GetLastName()
		{
			return $this->runetUser->LastName;
		}

		/**
		 * ОТЧЕСТВО ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return string
		 */
		public function GetFatherName()
		{
			return $this->runetUser->FatherName;
		}

		/**
		 * ТЕЛЕФОНЫ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return array
		 */
		public function GetPhones()
		{
			return $this->runetUser->Phones;
		}

		/**
		 * RoleID
		 *
		 * @return int
		 */
		public function GetRoleId()
		{
			if (!isset($this->runetUser->Status))
				return 0;
			else
				if (is_array($this->runetUser->Status))
					return $this->runetUser->Status;

			return $this->runetUser->Status->RoleId;
		}

		/**
		 * RoleName DEPRICATED
		 *
		 * @return string
		 */
		public function GetRoleName()
		{
			if (!isset ($this->runetUser->Status))
				return '';
			else
				if (is_array($this->runetUser->Status))
					return $this->runetUser->Status;

			return $this->runetUser->Status->RoleName;
		}

		/**
		 * RoleTitle
		 *
		 * @return string
		 */
		public function GetRoleTitle()
		{
			if (!isset ($this->runetUser->Status))
				return '';
			else
				if (is_array($this->runetUser->Status))
					return $this->runetUser->Status;

			return $this->runetUser->Status->RoleTitle;
		}

		/**
		 * ПУТИ К ФОТО ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return CRunetPhotoData
		 */
		public function GetPhoto()
		{
			return $this->runetUser->Photo;
		}

		/**
		 * ДОЛЖНОСТЬ ПОЛЬЗОВАТЕЛЯ
		 *
		 * @return string
		 */
		public function GetPosition()
		{
			return $this->runetUser->Work->Position;
		}

		/**
		 *
		 * @return CRunetCompanyData
		 */
		public function GetCompany()
		{
			return $this->runetUser->Work->Company;
		}

		/**
		 * УЧАСТВУЕТ В ТЕКУЩЕМ МЕРОПРИЯТИИ
		 *
		 * @return bool
		 */
		public function IsParticipant()
		{
			return isset($this->runetUser->Status);
		}

		/**
		 * Возвращает список целей меропрития для текущего пользователя
		 * @return array
		 */
		public function GetPurposes()
		{
			return CRunetGateUser::GetPurposes(self::GetRunetId());
		}

		/**
		 * Возвращает список проф. интересов для текущего пользователя
		 * @return array
		 */
		public function GetProfessionalInterests()
		{
			return CRunetGateUser::GetProfessionalInterests(self::GetRunetId());
		}
	}
