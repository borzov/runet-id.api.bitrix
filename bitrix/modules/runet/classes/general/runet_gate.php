<?php
IncludeModuleLangFile(__FILE__);

class CRunetGate {
  protected static $instance;

  public static $Debug = false;
  public static $DebugIp = array();
  public static $DebugHard = false;

  public static $LogMask = array();

  protected function __construct() {}  
  protected function __clone()     {}
  protected function __wakeup()    {}

  const GateDomain = 'http://api.runet-id.com/';  
    
  /**
    *
    * @return CRunetGate 
    */
  public static function Instance() 
  {
    if ( self::$instance === null ) 
    {
      self::$instance = new CRunetGate();
    }      
    return self::$instance;
  }
     
  /**
  *
  * @return boolean 
  */
  private static function IsDebug ()
  {
    return (self::$Debug && in_array($_SERVER['REMOTE_ADDR'], self::$DebugIp));
  }
    
  /**
    *
    * @param  string $url
    * @param  array  $vars
    * @return array 
    */
  public function Get ($url, $vars = array(), $cache = 0, $resetCache = false, $useAuth = true)
  {
    return self::Request('GET', $url, $vars, $cache, $resetCache, $useAuth);
  }

  /**
    *
    * @param  string $url
    * @param  array $vars
    * @return array 
    */
  public function Post ($url, $vars = array(), $cache = 0, $resetCache = false) 
  {
    return self::Request('POST', $url, $vars, $cache, $resetCache);
  }

  private function GetCacheId ($url, $vars)
  {
    return $url . serialize($vars);
  }

  /**
    *
    * @param  type $method
    * @param  type $url
    * @param  type $vars
    * @return type 
    */
  protected function Request ($method, $url, $vars = array(), $cache = 0, $resetCache = false, $useAuth = true) 
  {
    $startTime = microtime(true);
  
    // TODO: Привести url к единому формату
    if (!$url) 
    {
      throw new CRunetException( GetMessage ('RUNET.EXCEPTION.NOTURL'));
    }

    $bxCache = new CPHPCache();
    if ($resetCache)
    {
      $bxCache->Clean($this->GetCacheId($url, $vars), '/runet/');
    }

    if ($cache === 0 || !$bxCache->InitCache($cache, $this->GetCacheId($url, $vars), "/runet/"))
    {
      if ($useAuth)
      {
        $timestamp = time();
        $apikey    = COption::GetOptionString('runet', 'apikey');
        $secretkey = COption::GetOptionString('runet', 'secretkey');

        if (!$apikey || !$secretkey) 
        {
          //throw new CRunetException( GetMessage ('RUNET.EXCEPTION.NOTKEY'));
        } 

        $hash = substr( md5 ($apikey . $timestamp . $secretkey), 0, 16);
        $url .= '/?ApiKey='. $apikey .'&Timestamp='. $timestamp .'&Hash='. $hash;
      }
      else
      {
        $url .= '/?';
      }

      $vars = http_build_query($vars); 
      
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      switch ($method) 
      {
        case 'POST':
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $vars);
        break;

        case 'GET':
          $url .= '&'.$vars;
        break;
      }

      curl_setopt($curl, CURLOPT_URL, self::GateDomain . $url);
      $result = curl_exec($curl);

      if (self::IsDebug() && self::$DebugHard)
      {
        var_dump($result);
      }

      $result = json_decode($result);

      if (curl_errno($curl) == 0 && $cache !== 0 && !isset($result->Error))
      {
        $bxCache->StartDataCache();
        $bxCache->EndDataCache(array(
          'result' => $result
        ));
      }

      curl_close($curl);

    }
    else 
    {
      $bxCacheVars = $bxCache->GetVars();
      $result = $bxCacheVars['result'];
		}	

    $this->Log($url, $result, (microtime(true) - $startTime));

    if (self::IsDebug())
    {
      echo 'URL: '.$url.'<br/>';
      echo '<pre>';
      print_r($result);
      echo '</pre>';
    }
    return $result;
  }
	
	/**
	* ЛОГИРОВАНИЕ
	* 
	* @param mixed $url
	* @param mixed $result
	*/
	private function Log ($url, $result, $executionTime)
	{
		$writeToLog = false;
		if ( isset ($result->Error) && $result->Error)
		{
			$writeToLog = true;
		}
		else if (!empty(self::$LogMask))
		{
			foreach (self::$LogMask as $mask) 
			{
				if (strstr($url, $mask))
				{
					$writeToLog = true;
					break;
				}
			}
		}
		
		if ($writeToLog)
		{
			$logFilePath = $_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/runet/log/'. date('d-m-Y') .'.txt';
			file_put_contents(
				$logFilePath, 
				'----------------------------------------'. PHP_EOL .'DateTime: '. date('d-m-Y H:i:s') . PHP_EOL .'URL: '. $url . PHP_EOL . PHP_EOL . 'ExecutionTime: ' . $executionTime . PHP_EOL . PHP_EOL .'Result: '. PHP_EOL .''. var_export($result, true) . PHP_EOL .'----------------------------------------'. PHP_EOL . PHP_EOL . PHP_EOL,
				FILE_APPEND
			);
		}
	}
}
?>
