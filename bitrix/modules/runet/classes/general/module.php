<?php

	use Bitrix\Main\Config\Option;

	class CRunetModule
	{
		private static $arAddresses_withoutCheck = [
			'#^/bitrix/#',
			'#^/local/modules/runet/actions/check.email.php#',
			'#^/personal/#'
		];

		public static function OnProlog() {
			/** @var $GLOBALS cMain[]|cUser[] */
			global $APPLICATION, $USER;

			if (isset($_REQUEST['token'])) {
				if (!CRunetUser::Instance()->Login($_REQUEST['token'], true)) {
					ShowError('Неверный token авторизации');
					return;
				}

				if (!CRunetUser::Instance()->IsParticipant())
					CRunetEvent::Register(CRunetUser::Instance()->GetRunetId());

				?>
				<script type="text/javascript">
					window.opener.location.reload();
					window.close();
				</script>
				<?
				exit();
			}

			$APPLICATION->AddHeadString(sprintf('
				<script type="application/javascript">
					window.rIDAsyncInit = function() {
						rID.init({
							apiKey:"%s"
						});
					};

					// Load the SDK Asynchronously
					(function(d){
						var js, id = "runetid-jssdk", ref = d.getElementsByTagName("script")[0];
						if (d.getElementById(id)) {return;}
						js = d.createElement("script"); js.id = id; js.async = true;
						js.src = "//runet-id.com/javascripts/api/runetid.js";
						ref.parentNode.insertBefore(js, ref);
					}(document));
				</script>
			', COption::GetOptionString('runet', 'apikey')));

			if ($APPLICATION->sDirPath === '/email.check/') {
				$sHash = strrev(substr($APPLICATION->sDocPath2, 13));
				$iUserID = (int) $sHash;
				$sHashCode = substr(str_replace($iUserID, '', $sHash), 1);

				$arUser = CUser::GetByID($iUserID)
					->Fetch();

				if ($arUser['ACTIVE'] === 'Y')
					LocalRedirect('/?message=Ваш аккаунт уже активирован. Повторная активация не нужна.');

				if (substr(md5($arUser['CHECKWORD']), 32 - 6) === $sHashCode) {
					/* Активация аккаунта */
					(new CUser())
						->Update($iUserID, ['ACTIVE' => 'Y']);

					/* Автоматическая авторизация */
					if (!$USER->IsAuthorized())
						 $USER->Authorize($iUserID);

					LocalRedirect('/?message=Ваш аккаунт успешно активирован');
				}

				LocalRedirect('/?message=Ошибка активации E-Mail адреса');
			}

			if (!self::pragMatchesArray(self::$arAddresses_withoutCheck, $APPLICATION->sDocPath2) &&
				!$_REQUEST['message'] &&
				Option::get('runet', 'useAdditionalEmailCheck') === 'Y' &&
				$USER->IsAuthorized())
			{
				$arUser = CUser::GetByID($USER->GetID())->Fetch(); if ($arUser['ACTIVE'] !== 'Y') {
					$sRandomChars = str_split('qwertyuiopasdfghjklzxcvbnm');
					self::doSendEvent('RUNET_CHECK_EMAIL', [
						'EMAIL' => $arUser['EMAIL'],
						'FIO' => trim(implode(' ', [$arUser['LAST_NAME'], $arUser['NAME'], $arUser['SECOND_NAME']])),
						'URL' => sprintf('http://%s/email.check/%s',
							$_SERVER['HTTP_HOST'],
							strrev(implode([
								$arUser['ID'],
								$sRandomChars[array_rand($sRandomChars)],
								substr(md5($arUser['CHECKWORD']), 32 - 6)
							])))
					]);
					# Перенаправляем на страницу с сообщением об отправке проверочного письма
					LocalRedirect(file_exists($_SERVER['DOCUMENT_ROOT'].'/personal/check.email.php')
						? '/personal/check.email.php'
						: '/local/modules/runet/actions/check.email.php');
				}
			}
		}

		public static function OnBeforeUserAdd(&$arUser)
		{
			/** @var $GLOBALS cMain[] */
			if (COption::GetOptionString('runet', 'dataSync') == 'Y' && !self::GetRunetId_fromBitrixUser($arUser)) {
				$result = CRunetGateUser::Create($arUser['EMAIL'], $arUser['NAME'], $arUser['LAST_NAME'], array(
					'FatherName' => $arUser['SECOND_NAME'],
					'Company' => $arUser['WORK_COMPANY'],
					'City' => $arUser['WORK_CITY'] ? : $arUser['PERSONAL_CITY'],
					'Phone' => $arUser['WORK_PHONE'] ? : $arUser['PERSONAL_PHONE']
				));

				if (isset($result->Error)) {
					$GLOBALS['APPLICATION']->ThrowException($result->Error->Message);
					return false;
				}

				if (!CRunetUser::Instance()->Login($result->RunetId)) {
					$GLOBALS['APPLICATION']->ThrowException('Неверное имя пользователя');
					return false;
				}

				CRunetEvent::Register($result->RunetId);

				/* Запишем, для удобства, в поле "Внешний код" пользователя полученный RunetID */
				$arUser['XML_ID'] = sprintf('RUNETID:%d', $result->RunetId);
			}
			return true;
		}

		public static function OnBeforeUserUpdate(&$arUser)
		{
			/** @var $GLOBALS cMain[] */
			/* Возможность отправки изменённых данных о пользователе в API Битрикс. Противоречит
			 * политике безопасности и потому отключена. Хотя этот код может быть включен, но
			 * он всё равно не будет работать, так как редактирование данных отключено на стороне API. */
	//		if (COption::GetOptionString('runet', 'dataSync') == 'Y' && ($iRunetId = self::GetRunetId_fromBitrixUser($arUser))) {
	//			$result = CRunetGateUser::Edit($iRunetId, [
	//				'Email' => $arUser['EMAIL'],
	//				'FirstName' => $arUser['NAME'],
	//				'LastName' => $arUser['LAST_NAME'],
	//				'FatherName' => $arUser['SECOND_NAME'],
	//				'Company' => $arUser['WORK_COMPANY'],
	//				'City' => $arUser['WORK_CITY'] ? : $arUser['PERSONAL_CITY'],
	//				'Phone' => $arUser['WORK_PHONE'] ? : $arUser['PERSONAL_PHONE']
	//			]);
	//
	//			if (isset($result->Error)) {
	//				$GLOBALS['APPLICATION']->ThrowException($result->Error->Message);
	//				return false;
	//			}
	//		}
			return true;
		}

		public static function OnBeforeUserLogin(&$arCredentials, $isOAuthLogin = false) {
			/** @var $GLOBALS cMain[] */
			/* А позволяем ли мы авторизовываться посетителям без изспользования RunetID OAuth? */
			if (!$isOAuthLogin && $arCredentials['LOGIN'] != 'admin' && COption::GetOptionString('runet', 'preventSystemLogin') == 'Y') {
				if (preg_match('#^/bitrix/#', $GLOBALS['APPLICATION']->sDirPath))
					$GLOBALS['APPLICATION']->ThrowException('Разрешена исключительно RUNET-ID авторизация :(');
				else
					ShowError('Разрешена только RUNET-ID авторизация');
				return false;
			}
			return true;
		}

		public static function OnAfterUserLogin(&$arCredentials) {
			/** @var $GLOBALS cMain[] */
			/* Должны ли мы подгрузить данные из RunetId, заменив данные пользователя */
			if ($arCredentials['USER_ID'] && COption::GetOptionString('runet', 'dataSync') == 'Y') {
				$arUser = CUser::GetByID($arCredentials['USER_ID'])->Fetch(); if (($iRunetID = self::GetRunetId_fromBitrixUser($arUser))) {
					$arRunetUser = CRunetGateUser::Get($iRunetID);
					/* toDo: Тут можно и аватарку подгрузить... */
					$isUserChanged = false; // нам часто важно сохранять дату последнего изменения пользователя что бы не получилось так что он изменяется при каждом входе. Кто-то (или я не в столь поздний час) знает как не городить такой огород?
					if ($arUser['NAME'] != $arRunetUser->FirstName) { $isUserChanged = true; $arUser['NAME'] = $arRunetUser->FirstName; };
					if ($arUser['LAST_NAME'] != $arRunetUser->LastName) { $isUserChanged = true; $arUser['LAST_NAME'] = $arRunetUser->LastName; };
					if ($arUser['SECOND_NAME'] != $arRunetUser->FatherName) { $isUserChanged = true; $arUser['SECOND_NAME'] = $arRunetUser->FatherName; };
					if ($arUser['WORK_COMPANY'] != $arRunetUser->Work->Company->Name) { $isUserChanged = true; $arUser['WORK_COMPANY'] = $arRunetUser->Work->Company->Name; };
					if ($arUser['WORK_POSITION'] != $arRunetUser->Work->Position) { $isUserChanged = true; $arUser['WORK_POSITION'] = $arRunetUser->Work->Position; };
					if ($arUser['EMAIL'] != $arRunetUser->Email) { $isUserChanged = true; $arUser['EMAIL'] = $arRunetUser->Email; };
					if ($arUser['PERSONAL_PHONE'] != implode('<%phonesDelimiter%>', $arRunetUser->Phones)) { $isUserChanged = true; $arUser['PERSONAL_PHONE'] = implode('<%phonesDelimiter%>', $arRunetUser->Phones); };
					/* Обновляем пользователя, если были его изменения */
					if ($isUserChanged) {
						$obUser = new CUser();
						if (!$obUser->Update($arUser['ID'], $arUser)) {
							ShowError($obUser->LAST_ERROR);
						}
					}
				}
			}

			return true;
		}

		public static function GetRunetId_fromBitrixUser($arUser = null) {
			if ($arUser === null)
				$arUser = $GLOBALS['USER']->GetByID($GLOBALS['USER']->GetID())->Fetch();

			preg_match('#(?:runetId_|RUNETID:)(\d+)#', implode('|', [$arUser['LOGIN'], $arUser['XML_ID']]), $pregResult);

			return isset($pregResult[1])
				? $pregResult[1]
				: false;
		}

		/**
		 * @param $evType string Идентификатор почтового события
		 * @param $evDataSource array Ассоциативный массив параметров почтового шаблона и их значений
		 */
		private static function doSendEvent($evType, $evData)
		{
			/* Поля, параметры, которые всегда передаются в шаблон */
			$evTemplate_id = null;
			$evData = array_merge($evData, [
				'NAME' => 'NAME',
				'DATE_CREATE' => 'DATE_CREATE'
			]);

			if (strpos($evType, '|') !== false)
				list($evType, $evTemplate_id) = explode('|', $evType);

			/* Обработка входных данных */
			foreach ($evData as $key => &$value)
				$value = empty($value)
					? ' '
					: str_replace("\n", '<br>', trim(strip_tags($value)));

			$event = new CEvent();
			$event->SendImmediate($evType, SITE_ID == 'ru' ? 's1' : SITE_ID, $evData, 'Y', $evTemplate_id);
		}

		private static function pragMatchesArray($expressions, $string)
		{
			foreach ($expressions as $expression)
				if (preg_match($expression, $string))
					return true;

			return false;
		}
	}