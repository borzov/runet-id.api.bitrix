<?php
class CRunetPay
{
	/**
	 *
	 * @param  int $productId
	 * @param  int $payerRunetId
	 * @param  int $ownerRunetId
	 * @return CRunetOrderItemData
	 */
	public static function Add($productId, $payerRunetId, $ownerRunetId = null, $attributes = [])
	{
		if ($ownerRunetId === null) 
		{
			$ownerRunetId = $payerRunetId;
		}
		$result = CRunetGate::Instance()->Post('pay/add', array(
			'ProductId'  => $productId,
			'PayerRunetId' => $payerRunetId,
			'OwnerRunetId' => $ownerRunetId,
            'Attributes' => $attributes
		));
		return $result;
	}
    
	/**
	 * АКТИВАЦИЯ КУПОНА
	 *
	 * @param  string $couponCode
	 * @param  int $payerRunetId
	 * @param  int $ownerRunetId
	 * @return float 
	 */
	public static function Coupon ($couponCode, $payerRunetId, $ownerRunetId = null)
	{
		if ($ownerRunetId === null) 
	  {
	  	$ownerRunetId = $payerRunetId;
	  }
		$result = CRunetGate::Instance()->Post('pay/coupon', array(
			'CouponCode' => $couponCode,
			'PayerRunetId' => $payerRunetId,
			'OwnerRunetId' => $ownerRunetId
		));
		return $result;
		/*
		if (isset($result->Error) && $result->Error === true)
		{
	  	return 0;
		}
		else 
		{
	  	return $result->Discount;
		}
		*/
  }
    
    /**
    * Деактивация купона
    * 
    * @param string $couponCode
    * @param int $ownerRunetId
    */
    /*
    public static function CouponDelete ($couponCode, $ownerRunetId)
    {
			$result = CRunetGate::Instance()->Post('pay/coupon/delete', array(
				'CouponCode' => $couponCode,
			  'OwnerRunetId' => $ownerRunetId
			));
			return $result;
    }
    */
    
    /**
     *
     * @param  int $orderItemId
     * @param  int $payerRunetId
     * @return bool
     */
    public static function Delete($orderItemId, $payerRunetId)
    {
			$result = CRunetGate::Instance()->Post('pay/delete', array(
				'OrderItemId' => $orderItemId,
				'PayerRunetId'  => $payerRunetId
			));
			
//			return (isset($result->Error)) ? $result->Error : $result->Success;
			return $result;
    }
    
    /**
     *
     * @param int $payerRunetId
     * @param int $ownerRunetId 
     */
    public static function DeleteByOwnerRunetId ($payerRunetId, $ownerRunetId)
    {
    	$orderList = self::GetOrderList($payerRunetId);
      if ( !empty ($orderList->Items))
      {
      	foreach ($orderList->Items as $item) 
        {
        	if ($item->Owner->RunetId == $ownerRunetId)
          {
          	self::Delete($item->OrderItemId, $payerRunetId);
          }
        }
    	}
    }
    
    /**
     * ПОЛУЧЕНИЕ СПИСКА ЗАКАЗОВ ПОЛЬЗОВАТЕЛЯ
     *
     * @param  type $payerRunetId
     * @return stdClass
     */
    public static function GetOrderList ($payerRunetId)
    {
			$result = CRunetGate::Instance()->Get('pay/list', array(
				'PayerRunetId' => $payerRunetId
			));
			if (isset($result->Error) && $result->Error === true)
			{
				return null;
			}
	    return $result;
    }
    
    /**
     * @deprecated
     * @param type $payerRunetId 
     */
		/*
    public static function GetList ($payerRunetId)
    {
      return self::GetOrderList($payerRunetId);
    }
    */
    
    /**
     * СПИСОК ТОВАРОВ
     *
     * @return CRunetProductData[] 
     */
    public static function Products ()
    {
      $result = CRunetGate::Instance()->Get('pay/product', array(), 600);
      if (isset($result->Error) && $result->Error === true)
      {
        return null;
      }
      return $result;
    }
    
    /**
     *
     * @return int 
     */
    public static function GetDefaultProductId ()
    {
      return (int) COption::GetOptionString('runet', 'defaultProductId');
    }
    
    /**
     *
     * @param  int $payerRunetId
     * @return string 
     */
    public static function GetUrl ($payerRunetId)
    {
      $result = CRunetGate::Instance()->Get('pay/url', array(
        'PayerRunetId' => $payerRunetId 
      ), 604800);
      if (isset($result->Error) && $result->Error = true)
      {
        return null;
      }
      return $result->Url;
    }
    
    /**
    * ПОЛУЧЕНИЕ СПИСКА iTEMS (ТОВАРОВ) У ПОЛЬЗОВАТЕЛЯ Owner
    * 
    * @param int $ownerRunetId
    */
    public static function GetOrderItems($ownerRunetId)
    {
      $result = CRunetGate::Instance()->Get('pay/items', array(
        'OwnerRunetId' => $ownerRunetId 
      ));
      return $result;
    }
    
    /**
     * ПОЛУЧЕНИЕ ДАННЫХ ПО ПРОЖИВАНИЮ
     *
     * @param string $manager
     * @param array  $params
     * @param array $filter 
     */
    public static function FilterList ($manager, $params, $filter)
    {
        $result = CRunetGate::Instance()->Get('pay/filter/list', array(
            'Manager' => $manager,
            'Params'  => $params,
            'Filter'  => $filter
        ));
        
        if (isset($result->Error) && $result->Error = true)
        {
          return null;        
        }
        return $result;
    }
    
    /**
     * БРОНИРОВАНИЕ
     *
     * @param  string $manager
     * @param  array $params					// Фильтр бронирования
     * @param  int   $bookTime				// время брони для физ.лиц
     * @param  int   $payerRunetId
     * @param  int   $ownerRunetId
     * @return CRunetOrderItemData
     */
    public static function FilterBook ($manager, $params, $bookTime, $payerRunetId, $ownerRunetId = null)
    {
      if ($ownerRunetId === null) $ownerRunetId = $payerRunetId;
      $result = CRunetGate::Instance()->Get('pay/filter/book', array(
        'Manager'    => $manager,
        'Params'     => $params,
        'BookTime'   => $bookTime,
        'PayerRunetId' => $payerRunetId,
        'OwnerRunetId' => $ownerRunetId
      ));
      return $result;
    }


  public static function GetRooms($hotel = false)
  {
    $result = CRunetGate::Instance()->Get('pay/rifrooms', array());
    return $result;
  }

}
?>