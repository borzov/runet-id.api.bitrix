<?php
class CRunetCompany {

  /**
    * ДАННЫЕ О КОМПАНИИ ПО ID
    * @param int $companyId
    * @return array
    */
  static public function Get($companyID)
  {
    $result = CRunetGate::Instance()->Get('company/get', array('CompanyId' => $companyID));
    return $result;
  }







    
    /**
     *
     * @param  array $companyIds
     * @return array 
     */
    static public function GetList($companyIDs)
    {
        $companies = CRunetGate::Instance()->Get('company/list', array(
                'CompanyID' => implode(',', $companyIDs)
            ));
        
        $result = array();
        
        foreach($companies as $company)
        {
            $result[] = new CRunetCompany($company);
        }
        return $result;
    }
   
    
    /**
     *
     * @param  string $query
     * @param  int    $maxResults
     * @param  string $pageToken
     * @return array 
     */
    static public function Search($query, $maxResults = 200, $pageToken = '')
    {
       return
           CRunetGate::Instance()->Get('company/search', array(
               'Query'      => $query,
               'MaxResults' => $maxResults,
               'PageToken'  => $pageToken
           ));
    }
    
}
?>
