<?php require $_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php';

	$APPLICATION->SetTitle('Подтвердите регистрацию');

?>

<?=sprintf('На адрес <a href="mailto:%1$s">%1$s</a> было отправлено письмо со ссылкой для проверки корректности адреса', $USER->GetEmail())?>

<?require $_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'?>