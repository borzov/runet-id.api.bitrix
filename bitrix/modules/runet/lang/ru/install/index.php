<?php

	/** @var string[] $MESS */
	$MESS['RUNET.MODULE.NAME'] = 'RunetID';
	$MESS['RUNET.MODULE.DESCRIPTION'] = 'Модуль для работы с RunetID';
	$MESS['RUNET.RIGTH.DENIED'] = 'доступ закрыт';
	$MESS['RUNET.RIGTH.OPENED'] = 'доступ открыт';
	$MESS['RUNET.RIGTH.FULL'] = 'полный доступ';
	$MESS['RUNET.EVENT.CHECK.EMAIL'] = 'Проверка E-Mail';
	$MESS['RUNET.EVENT.CHECK.EMAIL.MESSAGE'] = 'Проверка E-Mail';
	$MESS['RUNET.EVENT.CHECK.EMAIL.MESSAGE.DESCR'] = 'Для активации аккаунта перейдите <a href="#URL#">по ссылке</a>';

