<?php
$MESS ['RUNET.OPTION.TAB.SETTINGS'] = 'Настройки';
$MESS ['RUNET.OPTION.TAB.SETTINGS_TITLE'] = 'Настройка параметров модуля';
$MESS ['RUNET.OPTION.APIKEY'] = 'API ключ';
$MESS ['RUNET.OPTION.SECRET_KEY'] = 'Секретный ключ пользователя';
$MESS ['RUNET.OPTION.DEFAULT_ROLEID'] = 'Роль, устанавливаемая по умолчанию при регистрации';
$MESS ['RUNET.OPTION.USE_BITRIX_GROUPS'] = 'Использовать группы Bitrix';
$MESS ['RUNET.OPTION.DATA_SYNC'] = 'Подгрузка данных пользователя из RUNET-ID<br><font style="font-size:11px;font-style:italic">(работает в момент авторизации посетителя с задержкой<br>из-за кеша на стороне api примерно в 5 мин.)</font>';
$MESS ['RUNET.OPTION.PREVENT_LOCAL_LOGIN'] = 'Запретить пользователям авторизоваться не через RunetID<br><font style="font-size:11px;font-style:italic">(этот запрет не распространяется на пользователя с логином admin)</font>';
$MESS ['RUNET.OPTION.DEFAULT_PRODUCTID'] = 'Продукт, добавляемый по умолчанию при регистрации';
$MESS ['RUNET.OPTION.ADDITIONAL_MAIL_CHECK'] = 'Доп. проверка E-Mail адреса после авторизации';
?>
