<script type="application/javascript">
  window.rIDAsyncInit = function() {
    rID.init({
      apiKey: '<?=COption::GetOptionString('runet', 'apikey');?>'
    });
  };

  // Load the SDK Asynchronously
  (function(d){
    var js, id = 'runetid-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//runet-id.com/javascripts/api/runetid.js";
    ref.parentNode.insertBefore(js, ref);
  }(document));
</script>

<?
CModule::IncludeModule('runet');
CRunetGate::$Debug = false;
CRunetGate::$DebugHard = false;
CRunetGate::$DebugIp[] = '82.142.129.35';
CRunetGate::$DebugIp[] = '10.10.1.46';
// Auth throw iframe
if (isset($_REQUEST['token']))
{
	$resultLogin = CRunetUser::Instance()->Login($_REQUEST['token'], true);
	if (!$resultLogin)
	{
		?>
		<script type="text/javascript">document.write('Неверный token авторизации');</script>
		<?
	}
	else
	{
		$objRunetUser = CRunetUser::Instance();
		if (!$objRunetUser->IsParticipant())
		{
			$resultRegister = CRunetEvent::Register($objRunetUser->GetRunetId());
		}
		?>
			<script type="text/javascript">
				window.opener.location.reload();
				window.close();
			</script>
		<?
		exit();
	}
}